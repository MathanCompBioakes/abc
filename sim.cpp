#include <stdlib.h>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <random>
#include <utility>
#include <algorithm>
#include <unordered_set>
#include "find_theta.h"

std::pair<int, int> select_t1_and_N() {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> dis(0, 1);
	auto r = dis(gen);
	int t = 0;
	int N = 0;
	if(r < 0.008982) {
		std::uniform_int_distribution<> dis2(501, 1250);
		N = 1200;
		t = dis2(gen);
	} else if (r < 0.41) {
		N = 12000;
		std::uniform_int_distribution<> dis2(1251, 5000);
		t = dis2(gen);
	} else {
		N = 120000;
		std::uniform_int_distribution<> dis2(100, 500);
		t = dis2(gen);
	}
	return std::make_pair(t, N);
}

void gen_results(const std::pair<int, int>& t_N, double s, Theta& theta, int z) {
	std::vector<std::string> lines = {"initialize() {\n",
		"       initializeMutationRate(1e-8);\n",
		"       initializeMutationType('m1', 0.5, 'f', 0.0);\n",
		"       initializeMutationType('m2', 0.5, 'f', " + std::to_string(s) + ");\n",
		"       initializeGenomicElementType('g1', m1, 1.0);\n",
		"       initializeGenomicElement(g1, 0, 99999);\n",
		"       initializeRecombinationRate(1e-8);\n",
		"}\n",
		"1 {\n",
		"       sim.addSubpop('p1', " + std::to_string(t_N.second) + ");\n",
		"       target = sample(p1.genomes, 1);\n",
		"       target.addNewDrawnMutation(m2, 10000);\n",
		"}\n",
		"1:" + std::to_string(t_N.first) + " late() {\n",
		"   mut = sim.mutationsOfType(m2);\n",
		"   if (size(mut) == 0) {\n",
		"    sim.simulationFinished();\n",
		"   } \n",
		"}\n"};
	if (t_N.first <= 500) {
	} else if (t_N.first <= 1250) {
		lines.push_back(std::to_string(t_N.first-500) + " { p1.setSubpopulationSize(120000); }\n");
	} else {
		lines.push_back(std::to_string(t_N.first-1250) + " { p1.setSubpopulationSize(1200); }\n");
		lines.push_back(std::to_string(t_N.first-500) + " { p1.setSubpopulationSize(120000); }\n");
	}
	lines.push_back(std::to_string(t_N.first) + " late() { sim.outputFull(); }\n");
	lines.push_back(std::to_string(t_N.first) + " late() { sim.outputFixedMutations(); }\n");
	std::ofstream file("tmp/tmp" + std::to_string(z) + ".txt");
	for (auto line : lines) {
		file << line;
	}
	file.close();
	std::string filename = "data/" + std::to_string(t_N.second) + "_" + std::to_string(t_N.first) + "_" + std::to_string(s) + ".txt";
	auto tmp = system(("/home/nathan/SLiM/bin/slim tmp/tmp" + std::to_string(z) + ".txt > " + filename).c_str());
	if(!tmp){}
	if(filesize(filename) > 800) {
		find_theta(filename, theta);
	}
	auto tmp2 = system(("rm " + filename).c_str());
	if(!tmp2){}
}

int main() {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> randNum(10, 1000);
	auto z = randNum(gen);
	while(true) {
		auto t_N = select_t1_and_N();
		auto theta = Theta();
		std::uniform_real_distribution<> dis(-0.01, 0.1);
		int count = 0;
		while((theta.frq < 0.6 || theta.frq > 0.8) && count < 1000) {
			auto s = dis(gen);
			gen_results(t_N, s, theta, z);
			count++;
		}
		if( theta.frq >= 0.6 && theta.frq <= 0.8) {
			std::cout << theta.t << "\t" << theta.s << "\t" << theta.frq << "\t" << 1.0/theta.L << "\t" << theta.M << "\t" << theta.R << std::endl;
		}
	}
	return 0;
}
