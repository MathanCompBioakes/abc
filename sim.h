#include <string>
#include <vector>
#include <unordered_set>
#include <fstream>

struct Theta {
	int t = 0;
	int length = 0;
	double L = 0;
	double R = 0;
	double M = 0;
	double s = 0;
	double frq = 0;
	std::string m2;
};

void find_theta(std::string filename, Theta& theta);
std::vector<std::pair<int, int>> createMatrix(std::ifstream& file, Theta& theta);
std::vector<std::unordered_set<int>> createHaplotypeList(std::ifstream& file, const Theta& theta);
std::ifstream::pos_type filesize(std::string filename);
