#include "find_theta.h"
#include <fstream>
#include <iostream>
#include <boost/algorithm/string.hpp>

int main(int argc, char* argv[]) {
	auto filename = argv[1];
	if(filesize(filename) >= 800) {
		auto theta = Theta();
		std::vector<std::string> strs;
		boost::split(strs, filename, boost::is_any_of("_"));
		theta.t = std::stoi(strs[1]);
		theta.s = std::stod(strs[2].substr(0, strs[2].size()-4));
		find_theta(argv[1], theta);
		if( theta.frq >= 0.6 && theta.frq <= 0.8) {
			std::cout << theta.t << "\t" << theta.s << "\t" << theta.frq << "\t" << 1.0/theta.L << "\t" << theta.M << "\t" << theta.R << std::endl;
		}
	}
	return 0;
}
