#include <stdlib.h>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <random>
#include <utility>
#include <algorithm>
#include <unordered_set>
#include "find_theta.h"

std::ifstream::pos_type filesize(std::string filename) {
	std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
	return in.tellg();
}

bool compare(const std::pair<int, int>&i, const std::pair<int, int>&j) {
	return i.second < j.second;
}

std::vector<std::pair<int, int>> createMatrix(std::ifstream& file, Theta& theta) {
	std::string line;
	std::vector<std::pair<int, int>> matrix;
	while(std::getline(file, line)) {
		if(boost::starts_with(line, "Individuals:")) {
			break;
		} else {
			std::vector<std::string> strs;
			boost::split(strs, line, boost::is_any_of("\t "));
			if(strs[1].compare("m2") == 0) {
				theta.m2 = strs[0];
				theta.frq = std::stoi(strs.back());
				theta.frq = theta.frq*1.0/240000;
				//if( theta.frq < 0.6 || theta.frq > 0.8 ){
				if( theta.frq == 0.0 ){
					return matrix;
				}
			}
			if(strs.back().compare("1") == 0) {
				theta.R++;
			}
			matrix.push_back(std::make_pair(std::stoi(strs[0]), std::stoi(strs[2])));
		}
	}
	std::sort(matrix.begin(), matrix.end(), compare);
	return matrix;
}

std::vector<std::unordered_set<int>> createHaplotypeList(std::ifstream& file, const Theta& theta) {
	std::vector<std::unordered_set<int>> haplotypeList;
	std::string line;
	std::vector<std::string> strs;
	while(std::getline(file, line)) {
		boost::split(strs, line, boost::is_any_of("\t "));
		if(std::find(strs.begin(), strs.end(), theta.m2) != strs.end()){
			std::unordered_set<int> tmp;
			for(auto i = strs.begin()+2; i < strs.end(); i++){
				tmp.insert(std::stoi(*i));
			}
			haplotypeList.push_back(tmp);
		}

	}
	return haplotypeList;
}

bool more_set(const std::pair<std::unordered_set<int>, int>& a,const std::pair<std::unordered_set<int>, int>& b) {
	   return a.first.size() > b.first.size();
}

void find_theta(std::string filename, Theta& theta) {
	std::ifstream file(filename);
	std::string line;
	std::vector<std::pair<int, int>> mutationDict;
	std::vector<std::unordered_set<int>> haplotypeList;
	while(std::getline(file, line)) {
		if(boost::starts_with(line, "initializeGenomicElement(")) {
			std::vector<std::string> strs;
			boost::split(strs, line, boost::is_any_of("\t "));
			strs[2].pop_back();
			strs[2].pop_back();
			theta.L = std::stoi(strs[2])*0.001;
			theta.length = theta.L;
		} else if(boost::starts_with(line, "Mutations:")) {
			mutationDict = createMatrix(file, theta);
			//if( theta.frq < 0.6 || theta.frq > 0.8 ) return;
			if( theta.frq < 0.0 ) return;
		} else if(boost::starts_with(line, "Genomes:")) {
			haplotypeList = createHaplotypeList(file, theta);
		}
	}
	std::unordered_set<int> mutations;
	for(unsigned i = 0; i < mutationDict.size(); ++i) {
		std::vector<std::pair<std::unordered_set<int>, int>> hapSet;
		mutations.insert(mutationDict[i].first);
		for(auto &haplotype : haplotypeList) {
			std::unordered_set<int> tmp;
			std::set_intersection(mutations.begin(), mutations.end(),
							haplotype.begin(), haplotype.end(),
							std::inserter(tmp, tmp.begin()));
			hapSet.push_back(std::make_pair(tmp, 0));
		}
		int total = 0;
		std::sort(hapSet.begin(), hapSet.end(), more_set);
		auto tmpList = haplotypeList;
		std::for_each(haplotypeList.begin(), haplotypeList.end(),
				[&hapSet](std::unordered_set<int>& list) {
					for(auto &set : hapSet) {
						bool subset = true;
						std::for_each(set.first.begin(), set.first.end(),
							[&list, &subset](int m){
								subset = subset&(list.find(m) != list.end());
							});
						if(subset) {
							set.second++;
							break;
						}
					}
				});
		for( auto& set : hapSet ){
			total += pow(set.second, 2)*1.0/2;
		}
		if( total*1.0/(pow(haplotypeList.size(), 2)*1.0/2) <= 0.05){
			theta.L = std::min(2*(mutationDict[i].first-1), theta.length*1000)*0.001;
			theta.R = theta.R*1.0/mutationDict.size();
			break;
		}
	}
	theta.M = mutationDict.size()*0.001/theta.L;
	std::vector<std::string> strs;
	boost::split(strs, filename, boost::is_any_of("_"));
	theta.t = std::stoi(strs[1]);
	theta.s = std::stod(strs[2].substr(0, strs[2].size()-4));
}
